//Ejercicio 1 - Escoger la frase correcta

$("#comprueba-one").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "3") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-two").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "2") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-three").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "3") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-four").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "1") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-five").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "2") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-six").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "2") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-seven").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "1") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});

$("#comprueba-eight").on("click", function() {
  var single = $(this).parent().find(".single").val();

  if (single.toLowerCase() == "3") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }

});
//Ejercicio 2 - Traduce al ingles

$("#comprueba-uno").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "hello how are you?") {
    alert("Good job, next");
  } else {
    alert("Incorrect");
  }
});

$("#comprueba-dos").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "i live in madrid with my family") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});

$("#comprueba-tres").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "i love eat cheese") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});

$("#comprueba-cuatro").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "i play football") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});

$("#comprueba-cinco").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "my name is pedro") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});

$("#comprueba-seis").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "i am ten years old") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});



//Ejercicio 3 - Deportes

$("#comprueba-football").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "football") {
    alert("Good job, let's continue");
  } else {
    alert("Nope");
  }
});

$("#comprueba-basketball").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "basketball") {
    alert("You are amazing");
  } else {
    alert("No, try again");
  }
});

$("#comprueba-tennis").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "tennis") {
    alert("Good, come on to the next one");
  } else {
    alert("Sorry but no");
  }
});

$("#comprueba-hockey").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "hockey") {
    alert("Nice job");
  } else {
    alert("Try another one");
  }
});

$("#comprueba-baseball").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "baseball") {
    alert("You got it");
  } else {
    alert("So bad");
  }
});

$("#comprueba-swim").on("click", function() {
  var texto = $(this).parent().find(".texto").val();

  if (texto.toLowerCase() == "swimming") {
    alert("Incredible");
  } else {
    alert("Oh, noo");
  }
});
